#ifndef CHANGETYPEPRICE_H
#define CHANGETYPEPRICE_H

#include <QDialog>

namespace Ui {
class changetypeprice;
}

class changetypeprice : public QDialog
{
    Q_OBJECT

public:
    explicit changetypeprice(QWidget *parent = 0);
    ~changetypeprice();

private slots:
    void on_btnchange_clicked();

    void on_btncancel_clicked();

private:
    Ui::changetypeprice *ui;

    bool getRoomType(QString room_type);
};

#endif // CHANGETYPEPRICE_H
