#include "accountmoney.h"
#include "ui_accountmoney.h"
#include <QtSql>
accountmoney::accountmoney(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::accountmoney)
{
    ui->setupUi(this);

    initview(1);
}

accountmoney::~accountmoney()
{
    delete ui;
}

void accountmoney::initview(int index)
{

    //实例化一个QChart对象
    QChart *chart = new QChart;
    ui->graphicsView->setChart(chart);  //将QChart对象设置到QChartView图表上
    //最近一个星期
    if(index == 1)
    {
        chart->setTitle("酒店月入收益图"); //设置标题
        //设置坐标轴
        QDateTimeAxis* axisX = new QDateTimeAxis;
        axisX->setTitleText("日期"); //设置标题
        axisX->setRange(QDateTime::currentDateTime().addMonths(-1), QDateTime::currentDateTime()); //设置范围
        int day =(int)QDateTime::currentDateTime().addMonths(-1).daysTo(QDateTime::currentDateTime());
        qDebug()<<day;
        axisX->setTickCount(day+1);      //设置主刻度个数
        axisX->setFormat("MM/dd");
        axisX->setLineVisible(true); //设置轴线和刻度线可见性
        axisX->setGridLineVisible(true); //设置网格线可见性

        QValueAxis* axisY = new QValueAxis;
        axisY->setTitleText("总和(百元)"); //设置标题

        QSqlQuery query1;
        QString sql1 = "select money from accountmoney where date between :first_time and DATE_ADD(:first_time,INTERVAL 1 Month)";
        query1.prepare(sql1);
        query1.bindValue(":first_time",QDate::currentDate().addMonths(-1));
        qDebug()<<"QDate::currentDate:"<<QDate::currentDate().toString(tr("MM/dd"));
        query1.exec();

        if(query1.size()<=0)
        {
            qDebug()<<"进入初始化插入阶段"<<endl;
            for(int q = 0; q<day; q++)
            {
                sql1 = "insert into accountmoney(money,date) value (0,:date)";
                query1.prepare(sql1);
                query1.bindValue(":date",QDate::currentDate().addMonths(-1).addDays(q));
                query1.exec();
            }
        }

        qDebug()<<"进入value赋值阶段"<<endl;
        double value[35]={0};
        for(int i = 0; i <= day;i++)
        {
            query1.next();
            if(query1.value(0).toDouble() != NULL)
            {
              value[i] = query1.value(0).toDouble()/100;
            }
            else
            {
              value[i] = 0;
            }

        }

        axisY->setRange(0, 100);       //设置范围
        axisY->setTickCount(26);      //设置主刻度个数
        axisY->setLineVisible(true); //设置轴线和刻度线可见性
        axisY->setGridLineVisible(true); //设置网格线可见性


        //设置序列1
        QLineSeries *series = new QLineSeries;
        series->setName("收入");    //设置序列名
        series->setColor(QColor(255,0,0)); //设置序列颜色

        QDate start_date = QDate::currentDate().addMonths(-1);
        QList<QDate> listDate;

        for(int i = 0; i <= day; i++)
        {
            listDate.append(start_date.addDays(i));
        }


        //添加数据点到序列
        for(int j = 0; j <= day; j++)
       {
            QDate dCurrDate = listDate.at(j);
            series->append(QPointF(QDateTime(dCurrDate).toMSecsSinceEpoch(),value[j]));
            qDebug()<<dCurrDate.toString(tr("MM/dd"))<<" + " <<value[j];
       }

        //给Qchart添加序列
        chart->addSeries(series);

        //把序列设置到坐标轴
        chart->setAxisY(axisY, series);
        chart->setAxisX(axisX, series);

    }
    //最近一个月
    else if(index == 2)
    {

    }
    //最近半年
    else if(index == 3)
    {

    }
    //最近一年
    else if(index == 4)
    {

    }
}
