#include "changetypeprice.h"
#include "ui_changetypeprice.h"
#include <QSqlQuery>
#include <QMessageBox>
#include <QDebug>
changetypeprice::changetypeprice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changetypeprice)
{
    ui->setupUi(this);
    this->setWindowTitle("更改房间价格");
}

changetypeprice::~changetypeprice()
{
    delete ui;
}

void changetypeprice::on_btnchange_clicked()
{
    QString type_name = ui->nametype->text().trimmed();
    QString type_price = ui->typeprice->text().trimmed();
    if(getRoomType(type_name)){
        QMessageBox::information(this,"提示","房间不存在！");
        close();
    }else{
        QSqlQuery query;
        QString sql = "update roomtype set type_price = :type_price where type_name = :type_name"; //修改信息数据库语句
        query.prepare(sql);
        query.bindValue(":type_name",type_name);
        query.bindValue(":type_price",type_price);
        if(query.exec()){
            QMessageBox::information(this,"提示","修改成功！");
            close();
        }else {
            QMessageBox::information(this,"提示","修改失败！");
            close();
        }
    }
}

void changetypeprice::on_btncancel_clicked()
{
    close();
}

bool changetypeprice::getRoomType(QString room_type)
{
    QString sql = "select * from roomtype where type_name = :room_type";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":room_type",room_type);
    query.exec();
    int size = query.size();
    qDebug()<<size;
    return size<=0;
}
