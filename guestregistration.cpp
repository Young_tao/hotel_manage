/*来客登记页面
 * 黄博
*/
#include "guestregistration.h"
#include "ui_guestregistration.h"
#include "saveloginuser.h"
#include <QSqlQuery>
#include <QStringList>
#include <QDebug>
#include <QDate>
#include <QMessageBox>
#include <QObject>
#include <QSqlRecord>
#include <QDateTime>
#include <QDateEdit>
GuestRegistration::GuestRegistration(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GuestRegistration)
{
    ui->setupUi(this);
    initRoomType();
    ui->outroom_time->setDate(QDate::currentDate().addDays(1));
    ui->inroom_tiem->setDate(QDate::currentDate());
}

GuestRegistration::~GuestRegistration()
{
    delete ui;
}

/*
 *函数功能：获取数据库中的房间类型并初始化组件
*/
void GuestRegistration::initRoomType()
{
    QSqlQuery query1;
    query1.prepare("select distinct type_name from roomtype");//去掉重复选项
    query1.exec();
    QStringList typeList;
    while(query1.next())
    {
        QString type = query1.value("type_name").toString();
        typeList << type;
    }
        ui->room_type_name->addItems(typeList);
}

/*
 *函数功能：通过入住时间和退房时间的变动生成入住天数
*/
void GuestRegistration::getInToOutTime(QDate inroom_time,QDate outroom_time)
{
    if(GuestRegistration::timeAccurate(inroom_time,outroom_time)){
        QString liveTime= QString::number(inroom_time.daysTo(outroom_time));
        ui->liveTiem->setText(liveTime);
    }else{
        QMessageBox::information(this,"提示","入住时间必须大于一天");
//        ui->inroom_tiem->setDate(ui->outroom_time->date().addDays(-1));
    }
}

/*
 *函数功能：获取入住时间和离开时间
*/
void GuestRegistration::getTime()
{
    QDate inroom_time = ui->inroom_tiem->date();//入住时间
    QDate outroom_time = ui->outroom_time->date();//退房时间
    getInToOutTime(inroom_time,outroom_time);
}

/*
 *函数功能：判断离开日期是否大于入住日期
*/
bool GuestRegistration::timeAccurate(QDate inroom_time,QDate outroom_time)
{
    if(inroom_time.daysTo(outroom_time)>0){
        return true;
    }else {
        return false;
    }
}

/*
 *函数功能：监听入住时间是否被改变，并计算出入住天数
*/
void GuestRegistration::on_inroom_tiem_dateChanged(const QDate &date)
{
    getTime();
}

/*
 *函数功能：监听离开时间是否被改变，并计算出离开时间
*/
void GuestRegistration::on_outroom_time_dateChanged(const QDate &date)
{
    getTime();
}

/*
 *函数功能：通过房间号查找判断房间是否已被使用
*/
bool GuestRegistration::theRoomExists(int room_no){
    QString value;
    QSqlQuery query;
    QString sql = "select room_state from room where room_no = :room_no";
    query.prepare(sql);
    query.bindValue(":room_no",room_no);
    query.exec();
    QSqlRecord rec = query.record();
    while (query.next()) {
        rec = query.record();
        value = query.value(0).toString();
    }
    return value=="空房";
}

/*
 *函数功能：通过房间号判断酒店是否存在该房间
*/
bool GuestRegistration::getRoom(int room_no){
    QString sql = "select * from room where room_no = :room_no";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":room_no",room_no);
    query.exec();
    int size = query.size();
    qDebug()<<size;
    return size<=0;
}
/*
 *函数功能：来客登记后改变房间状态
*/
void GuestRegistration::changingRoomStatus(int room_no){
    QString sql = "update room set room_state = '入住房' where room_no = :room_no";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":room_no",room_no);
    query.exec();
}

/*
 *函数功能：当房间号被改变，获取对应的房间类型
*/
void GuestRegistration::on_room_no_textChanged(const QString &arg1)
{
    int room_no = arg1.toInt();
    qDebug()<<room_no;
    QString sql = "select room_type from room where room_no = :room_no";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":room_no",room_no);
    query.exec();
    QSqlRecord rec = query.record();
    while (query.next()) {
       rec = query.record();
       QString value = query.value(0).toString();
       qDebug()<< value;
       ui->room_type_name->setCurrentText(value);
    }
}

/*
 *函数功能：添加来客登记信息
*/
void GuestRegistration::on_pbnCheckOk_clicked()
{
    int room_no = ui->room_no->text().trimmed().toInt();                //房间号
    QString room_type_name = ui->room_type_name->currentText();         //房间类型
    QDate inroom_time = ui->inroom_tiem->date();                        //入住时间
    QDate outroom_time = ui->outroom_time->date();                      //退房时间
    QString customer_name = ui->customer_name->text().trimmed();        //客户名字
    QString customer_sex = ui->customer_sex->currentText();             //客户性别
    QString customer_idcard = ui->customer_idcard->text().trimmed();    //客户身份证
    QString customer_phone = ui->customer_phone->text().trimmed();      //客户手机号
    QString inroom_cash = ui->inroom_cash->text().trimmed();            //入住押金
    QString Operator = SaveLoginUser::user_name;                        //操作员名字
    QDateTime inroom_realtime = QDateTime::currentDateTime();           //获取真实登记时间
    if(ui->room_no->text().trimmed() == ""){
        QMessageBox::information(this,"提示","必须填写房间号");
    }else if(customer_name == ""){
        QMessageBox::information(this,"提示","必须填写客户名字");
    }else if(customer_idcard == ""){
        QMessageBox::information(this,"提示","必须填写客户身份证");
    }else if(customer_phone == ""){
        QMessageBox::information(this,"提示","必须填写客户手机号");
    }else if(inroom_cash == ""){
        QMessageBox::information(this,"提示","必须填写入住押金");
    }else if(customer_idcard.length() != 18){
        QMessageBox::information(this,"提示","请正确填写十八位身份证号码");
    }else if(customer_phone.length() != 11){
        QMessageBox::information(this,"提示","请正确填写十一位手机号码");
    }else if(!timeAccurate(inroom_time,outroom_time)){
         QMessageBox::information(this,"提示","请确保离开日期大于入住日期");
    }else{
        if(getRoom(room_no)){
            QMessageBox::information(this,"提示","酒店不存在该房间!");
        }else if(!theRoomExists(room_no)){
            QMessageBox::information(this,"提示","房间已被使用！");
        }else{
            QSqlQuery query;
            QString sql = "insert into inroom(room_no,room_type_name,inroom_time,outroom_time,"
                          "customer_name,customer_sex,customer_idcard,customer_phone,"
                          "inroom_cash,operator,inroom_realtime) "
                          "VALUES (:room_no,:room_type_name,:inroom_time,:outroom_time,"
                          ":customer_name,:customer_sex,:customer_idcard,:customer_phone,"
                          ":inroom_cash,:operator,:inroom_realtime)";
            query.prepare(sql);
            query.bindValue(":room_no",room_no);                    //数据库非空
            query.bindValue(":room_type_name",room_type_name);
            query.bindValue(":inroom_time",inroom_time);
            query.bindValue(":outroom_time",outroom_time);
            query.bindValue(":customer_name",customer_name);        //数据库非空
            query.bindValue(":customer_sex",customer_sex);
            query.bindValue(":customer_idcard",customer_idcard);    //数据库非空
            query.bindValue(":customer_phone",customer_phone);      //数据库非空
            query.bindValue(":inroom_cash",inroom_cash);
            query.bindValue(":operator",Operator);
            query.bindValue(":inroom_realtime",inroom_realtime);
            query.exec();

            QSqlQuery query1;
            QString sql1 = "insert into `customer` (name,sex,idcard,phone) VALUES (:customer_name,:customer_sex,:customer_idcard,:customer_phone)";
            query1.prepare(sql1);
            query1.bindValue(":customer_name",customer_name);        //数据库非空
            query1.bindValue(":customer_sex",customer_sex);
            query1.bindValue(":customer_idcard",customer_idcard);    //数据库非空
            query1.bindValue(":customer_phone",customer_phone);      //数据库非空
            query1.exec();

            if(query.prepare(sql)){
                QMessageBox::information(this,"提示","来客登记成功");
                changingRoomStatus(room_no);
                ui->room_no->clear();
                ui->room_type_name->setCurrentText("单人房");
                ui->outroom_time->setDate(QDate::currentDate().addDays(1));
                ui->inroom_tiem->setDate(QDate::currentDate());
                ui->customer_name->clear();
                ui->customer_idcard->clear();
                ui->customer_phone->clear();
                ui->inroom_cash->clear();
            }
            else{
                QMessageBox::information(this,"提示","来客登记失败");
            }
        }
    }

}
