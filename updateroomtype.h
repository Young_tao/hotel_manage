#ifndef UPDATEROOMTYPE_H
#define UPDATEROOMTYPE_H

#include <QDialog>

namespace Ui {
class updateroomtype;
}

class updateroomtype : public QDialog
{
    Q_OBJECT

public:
    explicit updateroomtype(QWidget *parent = 0);
    ~updateroomtype();
signals:
    QString emitType();
private slots:
    void on_btn_update_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::updateroomtype *ui;
};

#endif // UPDATEROOMTYPE_H
