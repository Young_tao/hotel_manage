#ifndef ADDROOM_H
#define ADDROOM_H

#include <QDialog>

namespace Ui {
class addroom;
}

class addroom : public QDialog
{
    Q_OBJECT

public:
    explicit addroom(QWidget *parent = 0);
    ~addroom();

private slots:
    void on_btn_add_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::addroom *ui;

    void getRoomType();

    bool alreadyRoom(QString room_no);
};

#endif // ADDROOM_H
