#include "addroom.h"
#include "ui_addroom.h"
#include <QSqlQuery>
#include <QMessageBox>
addroom::addroom(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addroom)
{
    ui->setupUi(this);

    getRoomType();
}

addroom::~addroom()
{
    delete ui;
}

void addroom::on_btn_add_clicked()
{
    QString room_no = ui->room_no->text().trimmed();
    QString room_state = ui->room_state->currentText();
    QString room_type = ui->room_type->currentText();
    if(alreadyRoom(room_no)){
        QMessageBox::information(this,"提示","房间已存在！");
    }else {
        QSqlQuery query;
        QString sql = "insert into room(room_no,room_state,room_type) values(:room_no,:room_state,:room_type)"; //添加信息数据库语句
        query.prepare(sql);
        query.bindValue(":room_no",room_no);
        query.bindValue(":room_state",room_state);
        query.bindValue(":room_type",room_type);
        if(query.exec()){
            QMessageBox::information(this,"提示","添加成功！");
            close();
        }else {
            QMessageBox::information(this,"提示","添加失败！");
            close();
        }
    }
}

void addroom::on_btn_cancel_clicked()
{
    close();
}

void addroom::getRoomType()
{
    QSqlQuery query1;
    query1.prepare("select distinct type_name from roomtype");//去掉重复选项
    query1.exec();
    QStringList typeList;
    while(query1.next())
    {
        QString type = query1.value("type_name").toString();
        typeList << type;
    }
        ui->room_type->addItems(typeList);
}

bool addroom::alreadyRoom(QString room_no)
{
    QString sql = "select * from room where room_no = :room_no";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":room_no",room_no);
    query.exec();
    int size = query.size();
    return size>0;
}
