#include "addroomtype.h"
#include "ui_addroomtype.h"
#include <QSqlQuery>
#include <QMessageBox>
addRoomtype::addRoomtype(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addRoomtype)
{
    ui->setupUi(this);
    this->setWindowTitle("添加房间类型");
}

addRoomtype::~addRoomtype()
{
    delete ui;
}


void addRoomtype::on_btn_add_clicked()
{
    QString type_name = ui->type_name->text().trimmed();
    QString type_price = ui->type_price->text().trimmed();
    if(alreadyType(type_name)){
        QMessageBox::information(this,"提示","该类型已存在！");
    }else {
        QSqlQuery query;
        QString sql = "insert into roomtype(type_name,type_price) values(:type_name,:type_price)"; //添加信息数据库语句
        query.prepare(sql);
        query.bindValue(":type_name",type_name);
        query.bindValue(":type_price",type_price);
        if(query.exec()){
            QMessageBox::information(this,"提示","添加成功！");
            close();
        }else {
            QMessageBox::information(this,"提示","添加失败！");
            close();
        }
    }
}


void addRoomtype::on_btn_cancel_clicked()
{
    close();
}

bool addRoomtype::alreadyType(QString type_name)
{
    QString sql = "select * from roomtype where type_name = :type_name";
    QSqlQuery query;
    query.prepare(sql);
    query.bindValue(":type_name",type_name);
    query.exec();
    int size = query.size();
    return size>0;
}
