#ifndef GUESTREGISTRATION_H
#define GUESTREGISTRATION_H

#include <QWidget>
#include "saveloginuser.h"
namespace Ui {
class GuestRegistration;
}

class GuestRegistration : public QWidget
{
    Q_OBJECT

public:
    explicit GuestRegistration(QWidget *parent = 0);
    ~GuestRegistration();

private slots:
    void on_pbnCheckOk_clicked();
    void on_inroom_tiem_dateChanged(const QDate &date);
    void on_outroom_time_dateChanged(const QDate &date);
    void on_room_no_textChanged(const QString &arg1);

private:
    Ui::GuestRegistration *ui;

private:
    void initRoomType();
    void getInToOutTime(QDate inroom_time,QDate outroom_time);
    void getTime();
    bool timeAccurate(QDate inroom_time,QDate outroom_time);
    bool theRoomExists(int room_no);
    bool getRoom(int room_no);
    void changingRoomStatus(int room_no);
};

#endif // GUESTREGISTRATION_H
