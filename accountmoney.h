#ifndef ACCOUNTMONEY_H
#define ACCOUNTMONEY_H

#include <QWidget>
#include <QtCharts>
#include <QDateTime>
#include <QDate>
#include <QDateTimeAxis>
namespace Ui {
class accountmoney;
}

class accountmoney : public QWidget
{
    Q_OBJECT

public:
    explicit accountmoney(QWidget *parent = 0);
    ~accountmoney();

private:
    Ui::accountmoney *ui;

public slots:
    void initview(int index);
};

#endif // ACCOUNTMONEY_H
