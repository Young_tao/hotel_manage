#include "backroom.h"
#include "ui_backroom.h"
#include <QDebug>
backroom::backroom(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::backroom)
{
    ui->setupUi(this);
    initbox();
    initform();
    initview();
}

backroom::~backroom()
{
    delete ui;
}

void backroom::getPay(double pay)
{
    this->pay = pay;
}

void backroom::initbox(){
    ui->stackedWidget->setCurrentIndex(0);
}

void backroom::initform(){
    columnNames.clear();
    columnWidths.clear();

    tableName = "hotel.order";
    countName = "id";

    columnNames.append("订单号码");
    columnNames.append("客户姓名");
    columnNames.append("身份证号");
    columnNames.append("手机号码");
    columnNames.append("房间号码");
    columnNames.append("入住时间");
    columnNames.append("退房时间");
    columnNames.append("消费金额");
    columnNames.append("操作人员");
    columnNames.append("备注");

    columnWidths.append(80);
    columnWidths.append(80);
    columnWidths.append(120);
    columnWidths.append(80);
    columnWidths.append(80);
    columnWidths.append(120);
    columnWidths.append(120);
    columnWidths.append(80);
    columnWidths.append(80);
    columnWidths.append(120);

    //设置需要显示数据的表格和翻页的按钮
    dbPage = new DbPage(this);
    //设置所有列居中显示
    dbPage->setAllCenter(true);
    dbPage->setControl(ui->tableMain, ui->labPageTotal, ui->labPageCurrent, ui->labRecordsTotal, ui->labRecordsPerpage,
                       ui->labSelectTime, 0, ui->btnFirst, ui->btnPreVious, ui->btnNext, ui->btnLast, countName);
    ui->tableMain->horizontalHeader()->setStretchLastSection(true);
    ui->tableMain->verticalHeader()->setDefaultSectionSize(25);
}

void backroom::initview(){
    //绑定数据到表格
    QString sql = "where pay is null";
    dbPage->setConnName("hotel");
    dbPage->setTableName(tableName);
    dbPage->setOrderSql(QString("%1 %2").arg(countName).arg("desc"));
    dbPage->setWhereSql(sql);
    dbPage->setRecordsPerpage(20);
    dbPage->setColumnNames(columnNames);
    dbPage->setColumnWidths(columnWidths);
    dbPage->select();
}

void backroom::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug()<<"双击";
    }

}



void backroom::on_btn_roonno_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void backroom::on_btn_id_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void backroom::on_btnSelect_clicked()
{
    int index = ui->stackedWidget->currentIndex();
    if(index == 0)
    {
        QString noomno = ui->edit_roomno->text();
        if(noomno.isEmpty())
        {
            initview();
        }
        else
        {
            QString sql = "where roomno = " + noomno ;
            dbPage->setOrderSql(QString("%1 %2").arg(countName).arg("desc"));
            dbPage->setWhereSql(sql);
            dbPage->setRecordsPerpage(20);
            dbPage->select();
        }

    }
    else if(index == 1)
    {
        QString id = ui->edit_id->text();
        if(id.isEmpty())
        {
            initview();
        }
        else{
            QString sql = "where sql = " + id;
            dbPage->setOrderSql(QString("%1 %2").arg(countName).arg("desc"));
            dbPage->setWhereSql(sql);
            dbPage->setRecordsPerpage(20);
            dbPage->select();
        }

    }
    else
    {
        QMessageBox::warning(this,"查询失败","发生未知错误！");
    }
}

void backroom::on_btnbackroom_clicked()
{
    pay = -1;
    QModelIndexList selected = ui->tableMain->selectionModel()->selectedRows();
    if(selected.count()>1)
    {
        QMessageBox::warning(this,"退房失败","只能选择一个订单！");
    }
    else if(selected.count()<1)
    {
        QMessageBox::warning(this,"退房失败","请选择一个订单！");
    }else
    {
         QList<QModelIndex> list= ui->tableMain->selectionModel()->selectedRows();
         backroom_dialog *backlog = new backroom_dialog(this);
         //connect(this,SIGNAL(RoomNo(QString)),backlog,SLOT(getRMO(QString)));

         backlog->exec();

        if(pay == -1)
        {

        }
        else if(pay>0){
            //更新order订单
            QSqlQuery query1;
            QString sql1 = "update `order` set `outroom_time` = :outroom_time, `pay` = :pay where `id` = :id";
            query1.prepare(sql1);
            query1.bindValue(":outroom_time",QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"));
            query1.bindValue(":pay",pay);
            query1.bindValue(":id",list[0].data().toInt());
            query1.exec();
            //查找对应房间号进行置空
            QSqlQuery query2;
            QString sql2 = "select DISTINCT roomno from `order` where `id` = :id";
            query2.prepare(sql2);
            query2.bindValue(":id",list[0].data().toInt());
            query2.exec();
            query2.next();
            QString roomno = query2.value(0).toString();
            emit RoomNo(roomno);
            //更新房间状态为空房
            QSqlQuery query3;
            QString sql3 = "update `room` set `room_state` = '空房' where room_no = :roomno";
            query3.prepare(sql3);
            query3.bindValue(":roomno",roomno);
            query3.exec();
            //更新今日收入
            QSqlQuery query4;
            QString sql4 = "select money from accountmoney where date = :currentday";
            query4.prepare(sql4);
            query4.bindValue(":currentday",QDate::currentDate());
            query4.exec();
            query4.next();
            double account = query4.value(0).toDouble();
            if(account==0){
                QSqlQuery query;
                QString sql = "insert into accountmoney(money,date) values(:money,:date)"; //添加信息数据库语句
                query.prepare(sql);
                query.bindValue(":money",pay);
                query.bindValue(":date",QDate::currentDate());
                query.exec();
            }else{
                account += pay;
                sql4 = "update accountmoney set money = :currentmoney where date = :currentday";
                query4.prepare(sql4);
                query4.bindValue(":currentmoney",account);
                query4.bindValue(":currentday",QDate::currentDate());
                query4.exec();
            }
        }
        else{
            QMessageBox::warning(this,"支付失败","支付金额不能为0");
        }
         initview();
    }

}
