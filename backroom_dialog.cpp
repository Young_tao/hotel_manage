#include "backroom_dialog.h"
#include "ui_backroom_dialog.h"

backroom_dialog::backroom_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::backroom_dialog)
{
    ui->setupUi(this);
    connect(this,SIGNAL(emitPay(double)),parentWidget(),SLOT(getPay(double)));

    QRegExp exp("[0-9\\.-]+$");
    QValidator *Validator = new QRegExpValidator(exp);
    ui->lineEdit->setValidator(Validator);
    //ui->lineEdit->setText(rmo);
}

backroom_dialog::~backroom_dialog()
{
    delete ui;
}

void backroom_dialog::getRMO(QString RMO)
{
    this->rmo = RMO;
}

void backroom_dialog::on_pushButton_clicked()
{
    if(ui->lineEdit->text().isEmpty()){
        QMessageBox::warning(this,"失败","请输入金额");
    }
    else{
        double Pay = ui->lineEdit->text().toDouble();
        emit emitPay(Pay);
        QMessageBox::information(this,"提示","退房成功");
        close();
        qDebug()<<"支付成功！";
    }
}

void backroom_dialog::on_pushButton_2_clicked()
{
    emit emitPay(-1);
    close();
}
