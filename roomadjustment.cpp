#include "roomadjustment.h"
#include "ui_roomadjustment.h"
#include <QList>
#include <QDialog>
#include <QModelIndex>
roomadjustment::roomadjustment(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::roomadjustment)
{
    ui->setupUi(this);

    this->initForm();
    this->initView();
}

roomadjustment::~roomadjustment()
{
    delete ui;
}

QString roomadjustment::getType()
{
    QList<QModelIndex> list= ui->table_Main->selectionModel()->selectedRows();
    QString type = list[0].data().toString();
    return type;
}

void roomadjustment::initForm()
{
    columnNames.clear();
    columnWidths.clear();

    tableName = "hotel.roomtype";
    countName = "type_name";

    columnNames.append("房间类型");
    columnNames.append("房间价格/元");


    columnWidths.append(250);
    columnWidths.append(250);


    //设置需要显示数据的表格和翻页的按钮
    dbPage = new DbPage(this);
    //设置所有列居中显示
    dbPage->setAllCenter(true);
    dbPage->setControl(ui->table_Main, ui->lab_PageTotal, ui->lab_PageCurrent, ui->lab_RecordsTotal, ui->lab_RecordsPerpage,
                       ui->lab_SelectTime, 0, ui->btn_first, ui->btn_nextPage, ui->btn_prePage,ui->btn_end, countName);
    ui->table_Main->horizontalHeader()->setStretchLastSection(true);
    ui->table_Main->verticalHeader()->setDefaultSectionSize(25);
}

void roomadjustment::initView(){
    //绑定数据到表格
    QString sql = "where 1=1";
    dbPage->setConnName("hotel");
    dbPage->setTableName(tableName);
    dbPage->setOrderSql(QString("%1 %2").arg(countName).arg("desc"));
    dbPage->setWhereSql(sql);
    dbPage->setRecordsPerpage(20);
    dbPage->setColumnNames(columnNames);
    dbPage->setColumnWidths(columnWidths);
    dbPage->select();
}

/*
 *函数功能：添加新的房间类型以及价格
*/
void roomadjustment::on_btn_change_clicked()
{
    addRoomtype *addRoom = new addRoomtype(this);
    addRoom->exec();
    this->initView();
}

/*
 *函数功能：更改房间类型的价格
*/
void roomadjustment::on_btn_changePrice_clicked()
{
//    QList<QModelIndex> list= ui->tableMain->selectionModel()->selectedRows();
//    type=list[0].data();
    changetypeprice *change = new changetypeprice(this);
    change->exec();
    this->initView();
}

/*
 *函数功能：修改已有的房间类型
*/
void roomadjustment::on_btn_delete_clicked()
{
    QModelIndexList selected = ui->table_Main->selectionModel()->selectedRows();
    if(selected.count()>1)
    {
        QMessageBox::warning(this,"退房失败","只能选择一个订单！");
    }
    else if(selected.count()<1)
    {
        QMessageBox::warning(this,"退房失败","请选择一个订单！");
    }else {
        updateroomtype *update = new updateroomtype(this);
        update->exec();
        this->initView();
    }
}
