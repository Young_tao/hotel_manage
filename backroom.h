#ifndef BACKROOM_H
#define BACKROOM_H
#include "dbpage.h"
#include "backroom_dialog.h"
#include <QMouseEvent>
#include <QWidget>
#include <QMessageBox>
namespace Ui {
class backroom;
}

class backroom : public QWidget
{
    Q_OBJECT

public:
    explicit backroom(QWidget *parent = 0);
    ~backroom();
signals:
    void RoomNo(QString RMO);

public slots:
    void getPay(double pay);
    void initview();
    void initbox();
    void initform();
private slots:
    void mouseDoubleClickEvent(QMouseEvent *event);
    void on_btn_roonno_clicked();
    void on_btn_id_clicked();

    void on_btnSelect_clicked();

    void on_btnbackroom_clicked();

private:
    Ui::backroom *ui;
    double pay;
    QList<QString> columnNames; //字段名集合
    QList<int> columnWidths;    //字段宽度集合
    DbPage *dbPage;             //数据库翻页类
    QString tableName;          //表名称
    QString countName;          //统计行数字段名称
};

#endif // BACKROOM_H
