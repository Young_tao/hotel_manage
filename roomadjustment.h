#ifndef ROOMADJUSTMENT_H
#define ROOMADJUSTMENT_H

#include <QWidget>
#include "dbpage.h"
#include "addroomtype.h"
#include "changetypeprice.h"
#include <QMessageBox>
#include "updateroomtype.h"
namespace Ui {
class roomadjustment;
}

class roomadjustment : public QWidget
{
    Q_OBJECT

public:
    explicit roomadjustment(QWidget *parent = 0);
    ~roomadjustment();



private:
    Ui::roomadjustment *ui;

    QList<QString> columnNames; //字段名集合
    QList<int> columnWidths;    //字段宽度集合
    DbPage *dbPage;             //数据库翻页类
    QString type;
    QString tableName;          //表名称
    QString countName;          //统计行数字段名称

public slots:
    QString getType();
    void initForm();//表格初始化
    void initView();
//    void initbox();
private slots:
    void on_btn_change_clicked();
    void on_btn_changePrice_clicked();
    void on_btn_delete_clicked();
};

#endif // ROOMADJUSTMENT_H
