#ifndef BACKROOM_DIALOG_H
#define BACKROOM_DIALOG_H

#include <QDialog>
#include "QDebug"
#include "QMessageBox"
namespace Ui {
class backroom_dialog;
}

class backroom_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit backroom_dialog(QWidget *parent = 0);
    ~backroom_dialog();
signals:
    void emitPay(double pay);
public slots:
    void getRMO(QString RMO);
private slots:

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::backroom_dialog *ui;
    QString rmo;
};

#endif // BACKROOM_DIALOG_H
