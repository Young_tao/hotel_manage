#ifndef ADDROOMTYPE_H
#define ADDROOMTYPE_H

#include <QDialog>

namespace Ui {
class addRoomtype;
}

class addRoomtype : public QDialog
{
    Q_OBJECT

public:
    explicit addRoomtype(QWidget *parent = 0);
    ~addRoomtype();

private slots:

    void on_btn_add_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::addRoomtype *ui;

    bool alreadyType(QString type_name);
};

#endif // ADDROOMTYPE_H
