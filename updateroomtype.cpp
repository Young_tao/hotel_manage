#include "updateroomtype.h"
#include "ui_updateroomtype.h"
#include <QSqlQuery>
#include <QMessageBox>
#include "roomadjustment.h"
updateroomtype::updateroomtype(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::updateroomtype)
{
    ui->setupUi(this);
    connect(this,SIGNAL(emitType()),parentWidget(),SLOT(getType()));
    this->setWindowTitle("修改房间类型");
}

updateroomtype::~updateroomtype()
{
    delete ui;
}

void updateroomtype::on_btn_update_clicked()
{

        QString type_name = ui->type_name->text().trimmed();
        QString type_price = ui->type_price->text().trimmed();
        emit emitType();
        QString name = emitType();
        qDebug() << name;
        QString sql = "update roomtype set type_name = :update_name, type_price = :update_price where  type_name = :type_name";
        QSqlQuery query;
        query.prepare(sql);
        query.bindValue(":update_name",type_name);
        query.bindValue(":update_price",type_price);
        query.bindValue(":type_name",name);
        query.exec();
        if(query.exec()){
            QMessageBox::information(this,"提示","修改成功");
            close();
        }else {
            QMessageBox::information(this,"提示","修改失败");
            close();
        }
}

void updateroomtype::on_btn_cancel_clicked()
{
    close();
}
